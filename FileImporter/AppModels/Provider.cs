﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileImporter.AppModels
{

    class Provider : BaseEntity
    {
        public int ProviderId { get; set; }
        
        public string Name { get; set; }

        public List<File> Files { get; set; }
    }

}
