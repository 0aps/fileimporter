﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileImporter.AppModels
{

    class File : BaseEntity
    {
        public int FileId { get; set; }
 
        public String CustomId { get; set; }

        public String Name { get; set; }

        public byte[] Content { get; set; }

        public int ProviderId { get; set; }
        public Provider Provider { get; set; }
    }

}
