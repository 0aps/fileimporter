﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows.Forms;
using MetroFramework.Forms;

namespace FileImporter.AppForms
{
    public partial class ModernForm : MetroForm
    {
        public ModernForm()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(586, 462);
            this.BorderStyle = MetroFramework.Drawing.MetroBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Resizable = false;
        }

        public DialogResult DisplayForm(Form form)
        {
            DialogResult result = form.ShowDialog();
            return result;
        }
    }

}
