﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Drawing;


namespace FileImporter.AppForms
{
    public partial class MainForm : ModernForm
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            this.BorderStyle = MetroBorderStyle.FixedSingle;
        }

        private void registerTitle_Click(object sender, EventArgs e)
        {
            MainRegisterForm registerForm = new MainRegisterForm();
            this.DisplayForm(registerForm);
        }

        private void queryTitle_Click(object sender, EventArgs e)
        {
            QueryForm queryForm = new QueryForm();
            this.DisplayForm(queryForm);
        }
    }

}
