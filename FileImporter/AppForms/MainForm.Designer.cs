﻿namespace FileImporter.AppForms
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.registerTitle = new MetroFramework.Controls.MetroTile();
            this.queryTitle = new MetroFramework.Controls.MetroTile();
            this.SuspendLayout();
            // 
            // registerTitle
            // 
            this.registerTitle.Location = new System.Drawing.Point(30, 87);
            this.registerTitle.Name = "registerTitle";
            this.registerTitle.Size = new System.Drawing.Size(259, 330);
            this.registerTitle.TabIndex = 0;
            this.registerTitle.Text = "Registro";
            this.registerTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.registerTitle.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall;
            this.registerTitle.Click += new System.EventHandler(this.registerTitle_Click);
            // 
            // queryTitle
            // 
            this.queryTitle.Location = new System.Drawing.Point(295, 87);
            this.queryTitle.Name = "queryTitle";
            this.queryTitle.Size = new System.Drawing.Size(268, 330);
            this.queryTitle.TabIndex = 1;
            this.queryTitle.Text = "Consulta";
            this.queryTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.queryTitle.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall;
            this.queryTitle.Click += new System.EventHandler(this.queryTitle_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = MetroFramework.Drawing.MetroBorderStyle.FixedSingle;
            this.ClientSize = new System.Drawing.Size(586, 462);
            this.Controls.Add(this.queryTitle);
            this.Controls.Add(this.registerTitle);
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Resizable = false;
            this.Text = "Índice de archivos";
            this.TextAlign = System.Windows.Forms.VisualStyles.HorizontalAlign.Center;
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroTile registerTitle;
        private MetroFramework.Controls.MetroTile queryTitle;
    }
}