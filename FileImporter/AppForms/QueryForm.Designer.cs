﻿namespace FileImporter.AppForms
{
    partial class QueryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.providerComboBox = new MetroFramework.Controls.MetroComboBox();
            this.queryButton = new MetroFramework.Controls.MetroButton();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.fromLanbel = new MetroFramework.Controls.MetroLabel();
            this.toLabel = new MetroFramework.Controls.MetroLabel();
            this.providerLabel = new MetroFramework.Controls.MetroLabel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.metroButton1 = new MetroFramework.Controls.MetroButton();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // providerComboBox
            // 
            this.providerComboBox.FormattingEnabled = true;
            this.providerComboBox.ItemHeight = 23;
            this.providerComboBox.Location = new System.Drawing.Point(89, 115);
            this.providerComboBox.Name = "providerComboBox";
            this.providerComboBox.Size = new System.Drawing.Size(121, 29);
            this.providerComboBox.TabIndex = 0;
            // 
            // queryButton
            // 
            this.queryButton.Location = new System.Drawing.Point(477, 422);
            this.queryButton.Name = "queryButton";
            this.queryButton.Size = new System.Drawing.Size(86, 26);
            this.queryButton.TabIndex = 1;
            this.queryButton.Text = "Consultar";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(375, 81);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(188, 20);
            this.dateTimePicker1.TabIndex = 2;
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(87, 82);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker2.TabIndex = 3;
            // 
            // fromLanbel
            // 
            this.fromLanbel.AutoSize = true;
            this.fromLanbel.Location = new System.Drawing.Point(23, 83);
            this.fromLanbel.Name = "fromLanbel";
            this.fromLanbel.Size = new System.Drawing.Size(45, 19);
            this.fromLanbel.TabIndex = 4;
            this.fromLanbel.Text = "Desde";
            // 
            // toLabel
            // 
            this.toLabel.AutoSize = true;
            this.toLabel.Location = new System.Drawing.Point(311, 82);
            this.toLabel.Name = "toLabel";
            this.toLabel.Size = new System.Drawing.Size(41, 19);
            this.toLabel.TabIndex = 5;
            this.toLabel.Text = "Hasta";
            // 
            // providerLabel
            // 
            this.providerLabel.AutoSize = true;
            this.providerLabel.Location = new System.Drawing.Point(23, 120);
            this.providerLabel.Name = "providerLabel";
            this.providerLabel.Size = new System.Drawing.Size(64, 19);
            this.providerLabel.TabIndex = 6;
            this.providerLabel.Text = "Provedor";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(23, 175);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(540, 232);
            this.dataGridView1.TabIndex = 7;
            // 
            // metroButton1
            // 
            this.metroButton1.Location = new System.Drawing.Point(375, 422);
            this.metroButton1.Name = "metroButton1";
            this.metroButton1.Size = new System.Drawing.Size(86, 26);
            this.metroButton1.TabIndex = 8;
            this.metroButton1.Text = "Generar";
            // 
            // QueryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(586, 462);
            this.Controls.Add(this.metroButton1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.providerLabel);
            this.Controls.Add(this.toLabel);
            this.Controls.Add(this.fromLanbel);
            this.Controls.Add(this.dateTimePicker2);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.queryButton);
            this.Controls.Add(this.providerComboBox);
            this.Name = "QueryForm";
            this.Text = "Consulta de archivos";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroComboBox providerComboBox;
        private MetroFramework.Controls.MetroButton queryButton;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private MetroFramework.Controls.MetroLabel fromLanbel;
        private MetroFramework.Controls.MetroLabel toLabel;
        private MetroFramework.Controls.MetroLabel providerLabel;
        private System.Windows.Forms.DataGridView dataGridView1;
        private MetroFramework.Controls.MetroButton metroButton1;
    }
}