﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FileImporter.Helpers;
using FileImporter.Services;

namespace FileImporter.AppForms
{
    public partial class MainRegisterForm : ModernForm
    {
        ProviderService providerService;
        public MainRegisterForm()
        {
            providerService = new ProviderService();
            InitializeComponent();
        }

        private void registerFileForm_Click(object sender, EventArgs e)
        {
            RegisterFileForm registerFilesForm = new RegisterFileForm();
            this.DisplayForm(registerFilesForm);
        }

        private void registerProvider_Click(object sender, EventArgs e)
        {
            string promptValue = Prompt.ShowDialog("Nuevo proveedor");
            try
            {
                if(promptValue != "-1" && promptValue != "")
                {
                    providerService.AddNewProvider(promptValue);
                    MessageBox.Show("Provedor agregado correctamente.", "Notificación", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Notificación", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
