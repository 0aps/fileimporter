﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using FileImporter.Services;
using FileImporter.Helpers;
using FileImporter.AppModels;

namespace FileImporter.AppForms
{
    public partial class RegisterFileForm : ModernForm
    {
        ImporterService _importerService;
        ProviderService _providerService;
        List<ListViewItem> _listView;
        List<Image> _imageList;

        public RegisterFileForm()
        {
            InitializeComponent();
            CustomInitializeComponent();
        }

        private void CustomInitializeComponent()
        {
            listViewFiles.Columns.Add("Archivo", 150);
            listViewFiles.Columns.Add("Nombre", 150);
            listViewFiles.Columns.Add("Proveedor", 150);

            listViewFiles.AutoResizeColumn(2, ColumnHeaderAutoResizeStyle.HeaderSize);

            _importerService = new ImporterService();
            _providerService = new ProviderService();
            _imageList = new List<Image>();
            mainProviderComboBox.DataSource = _providerService.GetAll().ToList();
            mainProviderComboBox.DisplayMember = "Name";
            mainProviderComboBox.Invalidate();
        }

        private void loadFilesButton_Click(object sender, EventArgs e)
        {
            if (openFileDialogFiles.ShowDialog() == DialogResult.OK)
            {
                _listView = new List<ListViewItem>();
                totalFiles.Text = openFileDialogFiles.FileNames.Length + " archivos ...";
                String fileName;
                foreach (String file in openFileDialogFiles.FileNames)
                {
                    Image loadedImage = Image.FromFile(file);
                    _imageList.Add(loadedImage);
                    fileName = Path.GetFileName(file);

                    imageList1.Images.Add(fileName, loadedImage);

                    ListViewItem item = new ListViewItem();
                    item.SubItems.Add(fileName);
                    item.SubItems.Add("");
                    item.SubItems.Add("");
                    item.ImageKey = fileName;
                    _listView.Add(item);
                }

                listViewFiles.Items.AddRange( _listView.ToArray() );
            }
        }

        private void saveFilesButton_Click(object sender, EventArgs e)
        {
            try
            {
                List<AppModels.File> files = new List<AppModels.File>();
                AppModels.File file;
                foreach (ListViewItem item in _listView)
                {
                    int index = imageList1.Images.IndexOfKey(item.ImageKey);
                    Image image = _imageList[index];
                    file = new AppModels.File
                    {
                        Name = item.SubItems[1].Text,
                        Content = image.ToBytes(),
                        ProviderId = Int32.Parse(item.SubItems[3].Text)
                    };
                    files.Add(file);
                }

                int count = _importerService.AddNewFiles(files);
                if (count > 0)
                {
                    MessageBox.Show("Archivos agregados correctamente.", "Notificación", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
                else
                {
                    throw new Exception("No se agregó ningun archivo.");
                }
            }
            catch(NullReferenceException exc)
            {
                MessageBox.Show("No se agregó ningun archivo.", "Notificación", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "Notificación", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void mainProviderComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(mainProviderComboBox.Text) && listViewFiles.Items.Count > 0)
            {
                _listView.ForEach(item => {
                    item.SubItems[2].Text = mainProviderComboBox.Text;
                    item.SubItems[3].Text = ((Provider)mainProviderComboBox.SelectedItem).ProviderId.ToString();
                });
            }
        }
    }
}
