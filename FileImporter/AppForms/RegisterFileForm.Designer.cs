﻿namespace FileImporter.AppForms
{
    partial class RegisterFileForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.openFileDialogFiles = new System.Windows.Forms.OpenFileDialog();
            this.loadFilesButton = new MetroFramework.Controls.MetroButton();
            this.saveFilesButton = new MetroFramework.Controls.MetroButton();
            this.listViewFiles = new System.Windows.Forms.ListView();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.totalFiles = new MetroFramework.Controls.MetroTile();
            this.mainProviderComboBox = new MetroFramework.Controls.MetroComboBox();
            this.SuspendLayout();
            // 
            // openFileDialogFiles
            // 
            this.openFileDialogFiles.FileName = "openFileDialogFiles";
            this.openFileDialogFiles.Multiselect = true;
            // 
            // loadFilesButton
            // 
            this.loadFilesButton.Location = new System.Drawing.Point(458, 63);
            this.loadFilesButton.Name = "loadFilesButton";
            this.loadFilesButton.Size = new System.Drawing.Size(105, 36);
            this.loadFilesButton.TabIndex = 0;
            this.loadFilesButton.Text = "Cargar";
            this.loadFilesButton.Click += new System.EventHandler(this.loadFilesButton_Click);
            // 
            // saveFilesButton
            // 
            this.saveFilesButton.Location = new System.Drawing.Point(458, 412);
            this.saveFilesButton.Name = "saveFilesButton";
            this.saveFilesButton.Size = new System.Drawing.Size(105, 36);
            this.saveFilesButton.TabIndex = 1;
            this.saveFilesButton.Text = "Guardar";
            this.saveFilesButton.Click += new System.EventHandler(this.saveFilesButton_Click);
            // 
            // listViewFiles
            // 
            this.listViewFiles.LabelEdit = true;
            this.listViewFiles.Location = new System.Drawing.Point(23, 122);
            this.listViewFiles.Name = "listViewFiles";
            this.listViewFiles.Size = new System.Drawing.Size(540, 275);
            this.listViewFiles.SmallImageList = this.imageList1;
            this.listViewFiles.TabIndex = 2;
            this.listViewFiles.UseCompatibleStateImageBehavior = false;
            this.listViewFiles.View = System.Windows.Forms.View.Details;
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(100, 100);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // totalFiles
            // 
            this.totalFiles.Location = new System.Drawing.Point(25, 95);
            this.totalFiles.Name = "totalFiles";
            this.totalFiles.Size = new System.Drawing.Size(103, 23);
            this.totalFiles.TabIndex = 3;
            this.totalFiles.Text = "0 archivos ...";
            // 
            // mainProviderComboBox
            // 
            this.mainProviderComboBox.FormattingEnabled = true;
            this.mainProviderComboBox.ItemHeight = 23;
            this.mainProviderComboBox.Location = new System.Drawing.Point(299, 70);
            this.mainProviderComboBox.Name = "mainProviderComboBox";
            this.mainProviderComboBox.Size = new System.Drawing.Size(121, 29);
            this.mainProviderComboBox.TabIndex = 4;
            this.mainProviderComboBox.SelectedIndexChanged += new System.EventHandler(this.mainProviderComboBox_SelectedIndexChanged);
            // 
            // RegisterFileForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(586, 462);
            this.Controls.Add(this.mainProviderComboBox);
            this.Controls.Add(this.totalFiles);
            this.Controls.Add(this.listViewFiles);
            this.Controls.Add(this.saveFilesButton);
            this.Controls.Add(this.loadFilesButton);
            this.Name = "RegisterFileForm";
            this.Text = "Registro de archivos";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialogFiles;
        private MetroFramework.Controls.MetroButton loadFilesButton;
        private MetroFramework.Controls.MetroButton saveFilesButton;
        private System.Windows.Forms.ListView listViewFiles;
        private MetroFramework.Controls.MetroTile totalFiles;
        private System.Windows.Forms.ImageList imageList1;
        private MetroFramework.Controls.MetroComboBox mainProviderComboBox;
    }
}