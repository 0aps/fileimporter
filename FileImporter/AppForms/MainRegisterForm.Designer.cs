﻿namespace FileImporter.AppForms
{
    partial class MainRegisterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.registerFileForm = new MetroFramework.Controls.MetroTile();
            this.registerProvider = new MetroFramework.Controls.MetroTile();
            this.SuspendLayout();
            // 
            // registerFileForm
            // 
            this.registerFileForm.Location = new System.Drawing.Point(30, 87);
            this.registerFileForm.Name = "registerFileForm";
            this.registerFileForm.Size = new System.Drawing.Size(259, 330);
            this.registerFileForm.TabIndex = 1;
            this.registerFileForm.Text = "Archivos";
            this.registerFileForm.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.registerFileForm.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall;
            this.registerFileForm.Click += new System.EventHandler(this.registerFileForm_Click);
            // 
            // registerProvider
            // 
            this.registerProvider.Location = new System.Drawing.Point(295, 87);
            this.registerProvider.Name = "registerProvider";
            this.registerProvider.Size = new System.Drawing.Size(268, 330);
            this.registerProvider.TabIndex = 2;
            this.registerProvider.Text = "Provedores";
            this.registerProvider.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.registerProvider.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall;
            this.registerProvider.Click += new System.EventHandler(this.registerProvider_Click);
            // 
            // MainRegisterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(586, 462);
            this.Controls.Add(this.registerProvider);
            this.Controls.Add(this.registerFileForm);
            this.Name = "MainRegisterForm";
            this.Text = "Registro";
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroTile registerFileForm;
        private MetroFramework.Controls.MetroTile registerProvider;
    }
}