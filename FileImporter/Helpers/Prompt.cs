﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using MetroFramework.Drawing;

namespace FileImporter.Helpers
{
    public static class Prompt
    {
        public static string ShowDialog(string caption)
        {
            MetroForm prompt = new MetroForm()
            {
                Width = 500,
                Height = 150,
                FormBorderStyle = FormBorderStyle.None,
                Text = caption,
                Resizable = false,
                MaximizeBox = false,
                StartPosition = FormStartPosition.CenterScreen
            };

            TextBox textBox = new TextBox() { Left = 50, Top = 80, Width = 400 };
            Button confirmation = new Button() { Text = "OK", Left = 350, Width = 100, Top = 115, DialogResult = DialogResult.OK };
            confirmation.Click += (sender, e) => { prompt.Close(); };
            prompt.Controls.Add(textBox);
            prompt.Controls.Add(confirmation);
          
            prompt.AcceptButton = confirmation;

            return prompt.ShowDialog() == DialogResult.OK ? textBox.Text : "-1";
        }
    }
}
