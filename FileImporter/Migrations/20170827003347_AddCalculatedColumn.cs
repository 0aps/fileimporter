﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace FileImporter.Migrations
{
    public partial class AddCalculatedColumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Files",
                type: "TEXT",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CustomId",
                table: "Files",
                type: "TEXT",
                nullable: true,
                computedColumnSql: "DATE_FORMAT(CURRENT_DATE(), '%m%y'),  + '-' +[FileId]"
            );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Name",
                table: "Files");

            migrationBuilder.DropColumn(
                name: "CustomId",
                table: "Files");
        }
    }
}
