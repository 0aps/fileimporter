﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileImporter.AppModels;

namespace FileImporter.Services
{
    class ImporterService
    {
        ImporterContext ctx;
        public ImporterService()
        {
            ctx = new ImporterContext();
        }

        public int AddNewFiles(List<AppModels.File> files)
        {
            ctx.Files.AddRange(files);
            return ctx.SaveChanges();
        }
    }
}
