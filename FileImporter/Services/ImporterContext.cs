﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileImporter.AppModels;
using Microsoft.EntityFrameworkCore;

namespace FileImporter.Services
{
    class ImporterContext : DbContext
    {
        public DbSet<File> Files { get; set; }
        public DbSet<Provider> Providers { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            String cs = "C:\\Users\\Angel Santana\\Source\\Repos\\FileImporter\\FileImporter\\file_importer.db";
            optionsBuilder.UseSqlite("Data Source="+cs);
        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Provider>()
                 .HasIndex(p => p.Name)
                 .IsUnique();

            builder.Entity<File>()
                   .Property(p => p.CustomId)
                   .HasComputedColumnSql("DATE_FORMAT(CURRENT_DATE(), '%m%y'),  + '-' +[FileId]");
        }

        public override int SaveChanges()
        {
            AddTimestamps();
            return base.SaveChanges();
        }

        private void AddTimestamps()
        {
            var entities = ChangeTracker.Entries()
                .Where(x => x.Entity is BaseEntity && (x.State == EntityState.Added || x.State == EntityState.Modified));

            foreach (var entity in entities)
            {
                var now = DateTime.UtcNow;

                if (entity.State == EntityState.Added)
                {
                    ((BaseEntity)entity.Entity).CreatedAt = now;
                }
                ((BaseEntity)entity.Entity).UpdatedAt = now;
            }
        }

    }
}
