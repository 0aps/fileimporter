﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileImporter.AppModels;

namespace FileImporter.Services
{
    class ProviderService
    {
        ImporterContext ctx;
        public ProviderService() {
            ctx = new ImporterContext();
        }

        public int AddNewProvider(String providerName)
        {
            int count;
            Provider provider = new Provider { Name = providerName };
            ctx.Providers.Add(provider);
            count = ctx.SaveChanges();
            return count;
        }

        public IQueryable<Provider> GetAll()
        {
            return ctx.Providers;
        }

    }
}
